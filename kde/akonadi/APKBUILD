# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi
pkgver=18.12.2
pkgrel=0
pkgdesc="A cross-desktop storage service for PIM data and meta data providing concurrent read, write, and query access"
arch="all"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.1"
depends_dev="qt5-qtbase-dev kcompletion-dev kconfigwidgets-dev kdbusaddons-dev kiconthemes-dev kitemmodels-dev kio-dev shared-mime-info boost-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/applications/$pkgver/src/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # Requires running X11 server

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="a78ac757452360252eac2fcafbc01dcc5c84756db16da89c29c7ee8d7fa6deef3bc5b5bc1101c5e99e68b260fcdf1f96a780bef19b7d07198d4c00e546224431  akonadi-18.12.2.tar.xz"
